provider "aws" {
  region     = "ap-southeast-1"
  access_key = "AKIAQUMVXYBK6BEWZLP2"
  secret_key = "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"
}

variable "subnet_cidr_block" {
  description = "subnet cidr block"
  default = "10.0.10.0/24"
  type = string
}

variable "vpc_cidr_block" {
  description = "vps cidr block"
}

variable "environment" {
  description = "my env"
}

resource "aws_vpc" "development-vps" {
  cidr_block = var.vpc_cidr_block
  tags = {
    "Name"    = "development",
  }
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id            = aws_vpc.development-vps.id
  cidr_block        = var.subnet_cidr_block
  availability_zone = "ap-southeast-1a"
  tags = {
    "Name" : "subnet-1-dev"
  }
}
# ===================================
data "aws_vpc" "existing_vpc" {
  default = true
}

resource "aws_subnet" "dev-subnet-2" {
  vpc_id            = data.aws_vpc.existing_vpc.id
  cidr_block        = "172.31.48.0/20"
  availability_zone = "ap-southeast-1a"
  tags = {
    "Name" = "subnet-2-default"
  }
}

output "dev-vpc-id" {
  value = aws_vpc.development-vps.id
}
output "dev-subnet-id" {
  value = aws_subnet.dev-subnet-1
}
